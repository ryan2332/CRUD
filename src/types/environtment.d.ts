declare global {
    namespace NodeJS {
        interface ProcessEnv {
            NODE_PORT: number;
            POSTGRES_HOST: string;
            POSTGRES_PORT: number;
            POSTGRES_USER: string;
            POSTGRES_PASSWORD: string;
            POSTGRES_DATABASE: string;
            MONGODB_URI: string;
        }
    }
}

// If this file has no import/export statements (i.e. is a script)
// convert it into a module by adding an empty export statement.
export {};
