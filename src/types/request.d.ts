export type RequestWithSession = 
    Request & { session: Express.Session };