import { Request, Response, Router } from "express";
import { UserService } from "services/user";
import { RequestWithSession } from "types/request";


export class UserController {
    private router: Router;

    constructor (private userService:UserService){
        this.router = Router();
        this.router.get("/", this.getUser.bind(this));
        this.router.get("/:id", this.getUserProfile.bind(this));
        this.router.post("/add", this.registerUser.bind(this));
        this.router.patch('/user-update', this.updateUser.bind(this));
        this.router.delete('/delete', this.deleteUser.bind(this));
        this.router.delete('/soft-delete', this.softDeleteUser.bind(this));
        this.router.delete('/restore-soft-delete', this.restoreSoftDeleteUser.bind(this));
    }

    public getRouter() {
        return this.router;
    }

    public async getUser(req: Request, res: Response) {
        try {
            const users = await this.userService.getUser();

            return res.status(200).json(users);
        } catch (error) {
            return res.send(error);
        }
    }

    public async getUserProfile(req: Request, res: Response) {
        try {
            const users = await this.userService.getUserById(req.params.id);

            return res.status(200).json(users);
        } catch (error) {
            return res.send(error);
        }
    }

    public async registerUser(req: Request, res: Response){
        try {                                                                                                                                                                                                                                                                           
            const userToCreate= req.body;
            const user = await this.userService.registerUser(userToCreate);
            const { password, ...withoutPassword } = user;

            return res.status(200).json(withoutPassword);
        } catch (error) {
            return res.send(error);
        }
    }

    public async updateUser(req: RequestWithSession, res: Response) {
        const { user_id, ...userToUpdate} = req.body;

        console.log(user_id);
        console.log(userToUpdate);
        const users = await this.userService.getUserById(user_id);
        console.log(users);
        try {
                const user = await this.userService.updateUser(
                    user_id, userToUpdate
                );
                return res.status(200).json(user);

        } catch (error) {
            return res.status(500).json(error);
        }
    }

    public async deleteUser(req: RequestWithSession, res: Response) {
        const { user_id} = req.body;

        console.log(user_id);
        const users = await this.userService.getUserById(user_id);
        console.log(users);
        try {
                const user = await this.userService.deleteUser(
                    user_id
                );
                return res.status(200).json(user);

        } catch (error) {
            return res.status(500).json(error);
        }
    }

    public async softDeleteUser(req: RequestWithSession, res: Response) {
        const { user_id} = req.body;

        console.log(user_id);
        const users = await this.userService.getUserById(user_id);
        console.log(users);
        try {
                const user = await this.userService.softDeleteUser(
                    user_id
                );
                return res.status(200).json(user);

        } catch (error) {
            return res.status(500).json(error);
        }
    }

    public async restoreSoftDeleteUser(req: RequestWithSession, res: Response) {
        const { user_id} = req.body;

        console.log(user_id);
        const users = await this.userService.getUserById(user_id);
        console.log(users);
        try {
                const user = await this.userService.restoreSoftDeleteUser(
                    user_id
                );
                return res.status(200).json(user);

        } catch (error) {
            return res.status(500).json(error);
        }
    }
}