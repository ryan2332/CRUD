import { genSaltSync, hashSync } from "bcryptjs";
import { User } from "../entities/User";
import { UserRepository } from "repositories/user";

export class UserService {
    constructor(private userRepository : UserRepository){}

    async registerUser(userToCreate: Partial<User>): Promise<User> {
        const password = userToCreate.password;
        const takenEmail = await this.isEmailTaken(userToCreate.email);
        const saltRounds = 10;
        const salt = genSaltSync(saltRounds);
        const hashedPassword = hashSync(password, salt);
        userToCreate.password = hashedPassword;
        
        if (!takenEmail) {
            let createUser = await this.userRepository.create(userToCreate);
            createUser = await this.userRepository.insertUser(createUser);
            return createUser;
        }
        return null;
    }

    async isEmailTaken(email: string): Promise<boolean> {
        const isEmailExist = await this.userRepository.findByQuery({
            email,
        });
        if (isEmailExist.length === 1) {
            return true;
        }
        return false;
    }

    async getUser(): Promise<User[]> {
        const user = await this.userRepository.allUser();
        return user;
    }

    async getUserById(userId: string): Promise<User> {
        const user = await this.userRepository.findOne({
            id: userId,
        });

        return user;
    }

    async updateUser(userId: string, userToUpdate: Partial<User>): Promise<User> {
        let user: User = this.userRepository.create(userToUpdate);
        user = await this.userRepository.updateUser(userId, user);

        return user;
    }
    
    async deleteUser(userId: string): Promise<User> {
        const user = await this.userRepository.deleteUser(userId);

        return user;
    }
    
    async softDeleteUser(userId: string): Promise<User> {
        const user = await this.userRepository.softDeleteUser(userId);
        return user;
    }
    
    async restoreSoftDeleteUser(userId: string): Promise<User> {
        const user = await this.userRepository.restoreSoftDeleteUser(userId);
        return user;
    }
}