import { ConnectionOptions } from "typeorm";
import "./configs/env";

const {
    POSTGRES_HOST,
    POSTGRES_PORT,
    POSTGRES_USER,
    POSTGRES_PASSWORD,
    POSTGRES_DATABASE,
} = process.env;

export = {
    type: "postgres",
    host: POSTGRES_HOST || "localhost",
    port: POSTGRES_PORT || 5432,
    username: POSTGRES_USER || "postgres",
    password: POSTGRES_PASSWORD || "2332",
    database: POSTGRES_DATABASE || "postgres",
    synchronize: false,
    logging: true,
    entities: ["./src/entities/*"],
    migrations: ["./src/migrations/*"],
    subscribers: [],
    cli: {
        entitiesDir: "src/entities",
        migrationsDir: "src/migrations",
        subscribersDir: "src/subscriber",
    },
} as ConnectionOptions;
