import { User } from "../entities/User";
import { EntityRepository, Repository } from "typeorm";


export type FindUserQuery = {
    name?: string;
    email?: string;
    password?: string;
}

@EntityRepository(User)
export class UserRepository extends Repository<User>{
    public async insertUser(userToInsert: User): Promise<User> {
        const user = await this.save(userToInsert);
        return user;
    }

    public async allUser(): Promise<User[]> {
        const qb = this.createQueryBuilder("user");
        qb.withDeleted();
        const users = qb.getMany();
        return users;
    }

    public async findByQuery(userQuery: FindUserQuery): Promise<User[]> {
        const qb = this.createQueryBuilder("user");

        if (userQuery.name)
            qb.andWhere("user.name = :name", { name: userQuery.name });
        if (userQuery.password)
            qb.andWhere("user.password = password", { password: userQuery });
        if (userQuery.email)
            qb.andWhere("user.email = :email", { email: userQuery.email });

        const users = qb.getMany();
        return users;
    }

    public async updateUser(userId: string, userToUpdate: User): Promise<User> {
        const user = await this.update(userId, userToUpdate);
        return this.findOne({
            id: userId,
        })
    }
    

    public async deleteUser(userId: string): Promise<User> {
        const user = await this.delete(userId);
        return this.findOne({
            id: userId,
        })
    }
    

    public async softDeleteUser(userId: string): Promise<User> {
        const user = await this.softDelete(userId);
        return this.findOne({
            id: userId,
        })
    }

    public async restoreSoftDeleteUser(userId: string): Promise<User> {
        const user = await this.restore(userId);
        return this.findOne({
            id: userId,
        })
    }
}