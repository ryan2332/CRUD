import express, {Application} from "express";
import { config } from "dotenv";
import cors from 'cors';
import { getCustomRepository } from "typeorm";
import { connect } from "./utils/db-connector";
import { UserRepository } from "./repositories/user";
import { UserService } from "./services/user";
import { UserController } from "./controllers/user";

function setRoute(app: Application) {
    const userRepository = getCustomRepository(UserRepository);
    
    const userService = new UserService(userRepository);

    const userController = new UserController(userService);

    //------------------------------------------------------
    //ROUTE: neghubungin ke post dan get dkk
    app.get('/', (req,res) => {
        res.send('Home Base Server');
    });
    app.use("/users", userController.getRouter());

}

const cookieParser = require("cookie-parser");
const session = require("express-session");
const MongoStore = require("connect-mongo");

export async function createApp(): Promise<express.Application> {
    config();
    // load express module and create app
    const app = express();
    // app.use(cors);
    app.use(cookieParser());
    // using session and store to database
    // app.use(
    //     session({
    //         store: new MongoStore({
    //             mongoUrl: process.env.MONGODB_URI,
    //             collecton: "sessions",
    //             ttl: 14 * 24 * 60 * 60,
    //             autoRemove: "native",
    //         }),
    //         secret: "nogosari",
    //         resave: false,
    //         saveUninitialized: false,
    //         // set cookie to 1 week maxAge
    //         cookie: {
    //             maxAge: 1000 * 60 * 60 * 24 * 7,
    //             sameSite: true,
    //         },
    //     })
    // );
    app.set("port", process.env.NODE_PORT || 3000);

    app.use(express.json({ limit: "5mb", type: "application/json" }));
    app.use(express.urlencoded({ extended: true }));

    await connect();

    setRoute(app);

    return app;
}